<?php
require_once("bootstrap.php");
$gameData = json_decode(file_get_contents("php://input"), true)[0];
$tableName = "GameEntity";

$gameDataToSave = $gameData;
unset($gameDataToSave["onlineId"]);
$gameDataToSave["id"] = $dbh->getNewGameId();
$gameData["onlineId"] = $gameDataToSave["id"];

$dbh->create_table($gameDataToSave, $tableName);
echo json_encode(array($gameData));
?>