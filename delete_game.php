<?php
require_once("bootstrap.php");
$payload = json_decode(file_get_contents("php://input"), true);
$gameId =  $payload[0]["onlineId"];
$dbh->deleteTable("GameEntity", $gameId);
echo json_encode(array("ok"));
?>