<?php
require_once("bootstrap.php");
$payload = json_decode(file_get_contents("php://input"), true);
$gameId = $payload[0];
$gameData = $payload[1];
$dbh->createGame($gameId, $gameData);
echo json_encode(array("ok"));
?>