<?php
require_once("bootstrap.php");
$payload = json_decode(file_get_contents("php://input"), true);
$gameId =  $payload[0]["onlineId"];
$tables = array("GameEntity", "PlayerEntity");
$onlineGame = $dbh->getGameInfo($gameId, $tables);
echo json_encode(array($onlineGame));
?>