<?php
require_once("bootstrap.php");
$account = json_decode(file_get_contents("php://input"), true)[0];
if(empty($dbh->logIn($account["username"], $account["password"]))){
    echo json_encode(array(array("result"=>-1)));
}else{
    echo json_encode(array(array("result"=>1)));
}
?>